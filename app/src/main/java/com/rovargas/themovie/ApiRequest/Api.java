package com.rovargas.themovie.ApiRequest;

import com.rovargas.themovie.View.Main.Model.RequestMovies;
import com.rovargas.themovie.View.Details.Model.RequestMoviesDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @GET("movie/{type}?")
    Call<RequestMovies> GetMovie(@Path("type") String type, @Query("api_key") String apiKey,
                                 @Query("language") String language,
                                 @Query("page") int pageIndex);

    @GET("movie/{movie_id}")
    Call<RequestMoviesDetail> GetMovieDetails(@Path("movie_id") int movie_id, @Query("api_key") String apiKey,
                                              @Query("language") String language);

    @GET("search/movie")
    Call<RequestMovies> QueryMovie(@Query("api_key") String apiKey, @Query("language") String language, @Query("query") String query,  @Query("page") int pageIndex);
}
