package com.rovargas.themovie;

import android.app.Application;


public class ApplicationController extends Application {

    private static ApplicationController mInstance;

    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}
