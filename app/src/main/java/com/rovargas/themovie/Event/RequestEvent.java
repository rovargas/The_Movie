package com.rovargas.themovie.Event;

public class RequestEvent {

    private Type type;

    public RequestEvent() {
    }

    public RequestEvent(Type result) {
        type = result;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {
        SUCCESS,
        FAIL,
        NO_INTERNET,
    }

}
