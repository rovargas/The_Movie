package com.rovargas.themovie.View.Base.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.rovargas.themovie.R;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    //region variables
    private Toolbar mToolbar;
    //endregion


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        setupToolbar();
        initView();
    }

    /**
     * Use this method to initialize view components. This method is called after
     */
    public void initView() {
    }


    /**
     * Its common use a toolbar within activity, if it exists in the
     * layout this will be configured
     */
    public void setupToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }


    @Nullable
    public Toolbar getToolbar() {
        return mToolbar;
    }

    protected abstract int getLayoutId();


}
