package com.rovargas.themovie.View.Details.Presenter;

import android.content.Context;

import com.rovargas.themovie.ApplicationController;
import com.rovargas.themovie.Event.RequestEvent;
import com.rovargas.themovie.View.Base.Presenter.Presenter;
import com.rovargas.themovie.View.Details.Model.RequestMoviesDetail;
import com.rovargas.themovie.R;
import com.rovargas.themovie.ApiRequest.Api;
import com.rovargas.themovie.util.util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PresenterDetails extends Presenter<PresenterDetails.view> {

    public void initialize() {
        super.initialize();
    }

    public void GetMovie(int id) {
        if (util.isConnectingToInternet()) {

            getView().showLoading();

            String key = getContext().getString(R.string.Api_key);

            Api api = util.GetRetrofitBase().create(Api.class);
            api.GetMovieDetails(id, key, "es-co").enqueue(new Callback<RequestMoviesDetail>() {
                @Override
                public void onResponse(Call<RequestMoviesDetail> call, Response<RequestMoviesDetail> response) {
                    getView().hideLoading();
                    if (response.isSuccessful()) {
                        getView().ResultRequest(new RequestEvent(RequestEvent.Type.SUCCESS), response.body());
                    } else {
                        getView().ResultRequest(new RequestEvent(RequestEvent.Type.FAIL), response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<RequestMoviesDetail> call, Throwable t) {
                    getView().hideLoading();
                    getView().ResultRequest(new RequestEvent(RequestEvent.Type.FAIL), t.getMessage());
                }
            });


        } else {
            getView().ResultRequest(new RequestEvent(RequestEvent.Type.NO_INTERNET), "");
        }
    }


    public void destroy() {
        setView(null);
    }

    private Context getContext() {
        return ApplicationController.getInstance();
    }

    public interface view extends Presenter.View {

        void ResultRequest(RequestEvent event, Object result);

    }
}
