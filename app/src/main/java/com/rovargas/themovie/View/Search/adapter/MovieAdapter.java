package com.rovargas.themovie.View.Search.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rovargas.themovie.R;
import com.rovargas.themovie.View.Main.Model.Result;
import com.rovargas.themovie.View.Search.Presenter.PresenterSearch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //region variables
    private final PresenterSearch presenter;
    private List<Result> moviesList;
    private List<Result> filteredList;
    //endregion

    public MovieAdapter(PresenterSearch presenter) {
        this.presenter = presenter;
        moviesList = new ArrayList<>();
        filteredList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new itemViewHolderMovie(view, presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Result result = filteredList.get(position);
        itemViewHolderMovie itemViewHolderNotice = (itemViewHolderMovie) holder;
        itemViewHolderNotice.render(result);
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private void addItem(Result resultModel) {
        filteredList.add(resultModel);
        notifyDataSetChanged();
    }

    public void addAll(Collection<Result> collection) {
        moviesList.addAll(collection);
        filteredList.addAll(collection);
        notifyDataSetChanged();
    }

    public void ClearItem() {
        moviesList.clear();
        filteredList.clear();
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        filteredList.clear();
        if (charText.length() == 0) {
            filteredList.addAll(moviesList);
        } else {
            for (Result resultModel : moviesList) {
                if (resultModel.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    addItem(resultModel);
                }
            }
        }
        notifyDataSetChanged();
    }

}
