package com.rovargas.themovie.View.Details.View;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rovargas.themovie.Event.RequestEvent;
import com.rovargas.themovie.View.Base.View.BaseActivity;
import com.rovargas.themovie.View.Details.Model.RequestMoviesDetail;
import com.rovargas.themovie.View.Main.Model.Result;
import com.rovargas.themovie.View.Details.Model.genres;
import com.rovargas.themovie.View.Details.Presenter.PresenterDetails;
import com.rovargas.themovie.R;
import com.rovargas.themovie.util.AppDialogs;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity implements PresenterDetails.view {

    //region controlView
    @BindView(R.id.parallax_header_imageview)
    ImageView poster;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_tag)
    TextView tv_tag;

    @BindView(R.id.tv_date)
    TextView tv_date;

    @BindView(R.id.tv_time)
    TextView tv_time;

    @BindView(R.id.tv_overview)
    TextView tv_overview;

    @BindView(R.id.tv_gender)
    TextView tv_gender;

    @BindView(R.id.tv_vote_average)
    TextView tv_vote_average;

    @BindView(R.id.tv_adult)
    TextView tv_adult;

    @BindView(R.id.collapsing)
    CollapsingToolbarLayout collapsing;

    //endregion

    //region variables
    private ProgressDialog loading;
    private PresenterDetails presenter;

    //endregion

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail;
    }

    @Override
    public void initView() {
        super.initView();
        if (getToolbar() != null) {
            getToolbar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Result result = getIntent().getExtras().getParcelable("movie");
        String base = getString(R.string.UrlImage);

        Picasso.get().load(base + result.getPosterPath())
                .placeholder(R.drawable.img_movie)
                .error(R.drawable.img_movie)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        poster.setImageBitmap(bitmap);
                        Palette.from(bitmap)
                                .generate(new Palette.PaletteAsyncListener() {
                                    @Override
                                    public void onGenerated(Palette palette) {
                                        Palette.Swatch textSwatch = palette.getVibrantSwatch();
                                        if (textSwatch == null) {
                                            return;
                                        }
                                        collapsing.setBackgroundColor(textSwatch.getRgb());
                                    }
                                });
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

        initializePresenter(result.getId());
    }

    private void initializePresenter(int id) {
        presenter = new PresenterDetails();
        presenter.setView(this);
        presenter.GetMovie(id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void ResultRequest(RequestEvent event, Object result) {
        switch (event.getType()) {
            case NO_INTERNET:
                Toast.makeText(this, getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                break;
            case FAIL:
                Toast.makeText(this, result.toString(), Toast.LENGTH_SHORT).show();
                break;
            case SUCCESS:

                RequestMoviesDetail detail = (RequestMoviesDetail) result;

                tv_title.setText(detail.getOriginalTitle() + (" (Titulo Original)"));
                tv_date.setText(getString(R.string.premiere) + detail.getReleaseDate());
                tv_overview.setText(detail.getOverview());
                tv_tag.setText(detail.getTagline());
                tv_time.setText(detail.getRuntime() + " mint.");
                tv_vote_average.setText(detail.getVoteAverage().toString());
                String genre = "";
                for (genres genres : detail.getGenres()) {
                    genre += genres.getName() + " | ";
                }
                tv_gender.setText(genre);
                String adult = getString(R.string.adult);
                if (detail.getAdult()) {
                    adult += getString(R.string.yes);
                } else {
                    adult += getString(R.string.not);
                }
                tv_adult.setText(adult);
                break;
        }
    }

    @Override
    public void showLoading() {
        loading = AppDialogs.createLoading(this);
    }

    @Override
    public void hideLoading() {
        if (loading != null)
            loading.dismiss();
    }
}
