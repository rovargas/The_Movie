package com.rovargas.themovie.View.Main.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rovargas.themovie.ApplicationController;
import com.rovargas.themovie.View.Main.Model.Result;
import com.rovargas.themovie.View.Main.Presenter.PresenterMain;
import com.rovargas.themovie.R;
import com.squareup.picasso.Picasso;

public class itemViewHolderMovie extends RecyclerView.ViewHolder {

    private final PresenterMain presenter;

    itemViewHolderMovie(View view, PresenterMain presenter) {
        super(view);
        this.presenter = presenter;
    }

    public void render(Result itemModel) {
        onItemClick(itemModel);
        renderTitle(itemModel.getTitle());
        renderImage(itemModel.getBackdropPath());
    }

    private void onItemClick(final Result FieldViewModel) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onMovieClicked(FieldViewModel);
            }
        });
    }

    private void renderTitle(String name) {
        TextView txtname = itemView.findViewById(R.id.tv_title);
        txtname.setText(name);
    }

    private void renderImage(String url) {
        ImageView poster = itemView.findViewById(R.id.img_poster);
        String base = ApplicationController.getInstance().getString(R.string.UrlImage);
        Picasso.get().load(base + url)
                .placeholder(R.drawable.img_movie)
                .error(R.drawable.img_movie)
                .into(poster);
    }

}
