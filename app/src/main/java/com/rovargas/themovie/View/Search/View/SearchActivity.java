package com.rovargas.themovie.View.Search.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.rovargas.themovie.Event.RequestEvent;
import com.rovargas.themovie.R;
import com.rovargas.themovie.View.Base.View.BaseActivity;
import com.rovargas.themovie.View.Details.View.DetailActivity;
import com.rovargas.themovie.View.Main.Model.RequestMovies;
import com.rovargas.themovie.View.Main.Model.Result;
import com.rovargas.themovie.View.Search.Presenter.PresenterSearch;
import com.rovargas.themovie.View.Search.adapter.MovieAdapter;
import com.rovargas.themovie.util.AppDialogs;
import com.rovargas.themovie.util.EndlessRecyclerViewScrollListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends BaseActivity implements PresenterSearch.view {

    //region ControlView
    @BindView(R.id.list_movie)
    RecyclerView recyclerView;
    //endregion

    //region Variables
    private ProgressDialog loading;
    private MovieAdapter adapter;
    private PresenterSearch presenter;
    private String query = "";
    //endregion

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    public void initView() {
        super.initView();
        if (getToolbar() != null) {
            getToolbar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initializePresenter();
        initializeAdapter();
        SearchView edit_query = findViewById(R.id.edit_query);
        edit_query.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                query = s;
                presenter.GetMovie(s, 1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                query = s;
                presenter.GetMovie(s, 1);
                return false;
            }
        });
    }

    private void initializeAdapter() {
        adapter = new MovieAdapter(presenter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        GridLayoutManager manager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                presenter.GetMovie(query, page + 1);
            }
        });

    }

    private void initializePresenter() {
        presenter = new PresenterSearch();
        presenter.setView(this);
    }

    @Override
    public void onMovieClicked(Result FieldViewModel) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("movie", FieldViewModel);
        startActivity(intent);
    }

    @Override
    public void ResultRequest(RequestEvent event, Object result) {
        switch (event.getType()) {
            case SUCCESS:
                RequestMovies requestMovies = (RequestMovies) result;
                if (requestMovies.getPage() == 1) {
                    adapter.ClearItem();
                }
                adapter.addAll(requestMovies.getResults());
                break;

            case FAIL:
                Toast.makeText(this, result.toString(), Toast.LENGTH_SHORT).show();
                break;

            case NO_INTERNET:
                Toast.makeText(this, getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void ClearList() {
        adapter.ClearItem();
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
