package com.rovargas.themovie.View.Main.Presenter;

import android.content.Context;


import com.rovargas.themovie.ApplicationController;
import com.rovargas.themovie.Event.RequestEvent;
import com.rovargas.themovie.View.Main.Model.RequestMovies;
import com.rovargas.themovie.View.Main.Model.Result;
import com.rovargas.themovie.View.Base.Presenter.Presenter;
import com.rovargas.themovie.R;
import com.rovargas.themovie.ApiRequest.Api;
import com.rovargas.themovie.util.util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterMain extends Presenter<PresenterMain.view> {


    @SuppressWarnings("unchecked")
    @Override
    public void initialize() {
        super.initialize();
    }

    public void GetMovie(boolean isRefresh, String type, int page) {
        if (util.isConnectingToInternet()) {
            if (!isRefresh)
                getView().showLoading();

            String key = getContext().getString(R.string.Api_key);

            Api api = util.GetRetrofitBase().create(Api.class);
            api.GetMovie(type, key, "es-co", page).enqueue(new Callback<RequestMovies>() {
                @Override
                public void onResponse(Call<RequestMovies> call, Response<RequestMovies> response) {
                    getView().hideLoading();
                    if (response.isSuccessful()) {
                        getView().ResultRequest(new RequestEvent(RequestEvent.Type.SUCCESS), response.body());
                    } else {
                        getView().ResultRequest(new RequestEvent(RequestEvent.Type.FAIL), response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<RequestMovies> call, Throwable t) {
                    getView().hideLoading();
                    getView().ResultRequest(new RequestEvent(RequestEvent.Type.FAIL), t.getMessage());
                }
            });

        } else {
            getView().ResultRequest(new RequestEvent(RequestEvent.Type.NO_INTERNET), "");
        }
    }

    public void destroy() {
        setView(null);
    }

    private Context getContext() {
        return ApplicationController.getInstance();
    }

    public void onMovieClicked(Result FieldViewModel) {
        getView().onMovieClicked(FieldViewModel);
    }

    public interface view extends Presenter.View {

        void onMovieClicked(Result FieldViewModel);

        void ResultRequest(RequestEvent event, Object result);

    }

}

