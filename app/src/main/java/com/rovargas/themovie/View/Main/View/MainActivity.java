package com.rovargas.themovie.View.Main.View;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.rovargas.themovie.View.Base.View.BaseActivity;
import com.rovargas.themovie.View.Main.Adapter.MovieAdapter;
import com.rovargas.themovie.Event.RequestEvent;
import com.rovargas.themovie.View.Main.Model.RequestMovies;
import com.rovargas.themovie.View.Main.Model.Result;
import com.rovargas.themovie.View.Main.Presenter.PresenterMain;
import com.rovargas.themovie.R;
import com.rovargas.themovie.View.Details.View.DetailActivity;
import com.rovargas.themovie.View.Search.View.SearchActivity;
import com.rovargas.themovie.util.AppDialogs;
import com.rovargas.themovie.util.EndlessRecyclerViewScrollListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements PresenterMain.view {

    //region ControlView
    @BindView(R.id.list_movie)
    RecyclerView recyclerView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //endregion

    //region variables
    private ProgressDialog loading;
    PresenterMain presenter;
    TypeCategory typeItem;
    MovieAdapter adapter;
    private String valueQuery = "";
    //endregion

    //region enum
    public enum TypeCategory {
        upcoming,
        popular,
    }
    //endregion

    //region OnNavigationItemSelectedListener
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_popular:
                    typeItem = TypeCategory.popular;
                    break;
                case R.id.navigation_upcoming:
                    typeItem = TypeCategory.upcoming;
                    break;
                case R.id.navigation_search:
                    startActivity(new Intent(MainActivity.this, SearchActivity.class));
                    return true;
            }
            presenter.GetMovie(false, typeItem.name(), 1);
            return true;
        }
    };

    //endregion

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        super.initView();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initializePresenter();
        initializeAdapter();
        initializeSwipeRefresh();
        navigation.setSelectedItemId(R.id.navigation_popular);
    }

    private void initializePresenter() {
        presenter = new PresenterMain();
        presenter.setView(this);
    }

    private void initializeAdapter() {
        adapter = new MovieAdapter(presenter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        GridLayoutManager manager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                presenter.GetMovie(false, typeItem.name(), page + 1);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void initializeSwipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.ClearItem();
                presenter.GetMovie(true, typeItem.name(), 1);
            }
        });
    }

    @Override
    public void onMovieClicked(Result FieldViewModel) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("movie", FieldViewModel);
        startActivity(intent);
    }

    @Override
    public void ResultRequest(RequestEvent event, Object result) {
        switch (event.getType()) {
            case SUCCESS:
                RequestMovies requestMovies = (RequestMovies) result;
                if (requestMovies.getPage() == 1) {
                    adapter.ClearItem();
                }
                adapter.addAll(requestMovies.getResults());
                adapter.filter(valueQuery);
                break;

            case FAIL:
                Toast.makeText(this, result.toString(), Toast.LENGTH_SHORT).show();
                break;

            case NO_INTERNET:
                Toast.makeText(this, getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void showLoading() {
        if (loading == null)
            loading = AppDialogs.createLoading(this);
    }

    @Override
    public void hideLoading() {
        if (loading != null) {
            loading.dismiss();
            loading = null;
        }

        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_filter);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    valueQuery = query;
                    adapter.filter(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    valueQuery = newText;
                    adapter.filter(newText);
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }


}
