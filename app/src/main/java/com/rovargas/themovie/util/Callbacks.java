package com.rovargas.themovie.util;

/**
 * Created by moebius on 30/11/15.
 */
public interface Callbacks {

    void onSuccess(Object data);

    void onFail(String message);
}
