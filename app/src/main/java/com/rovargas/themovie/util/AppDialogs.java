package com.rovargas.themovie.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.rovargas.themovie.R;

import java.util.List;

/**
 * Created by moebius on 5/12/15.
 */
public class AppDialogs {

    public static ProgressDialog createLoading(Context context) {
        return ProgressDialog.show(context, "", "Espere un momento...", true);
    }

    public static Dialog createDialog(Context context, final String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton("Ok!", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        return builder.create();
    }

    public static Dialog createDialog(Context context, final String title, final String message, final Callbacks callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("Ok!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.onSuccess("success");
                    }
                });
        return builder.create();
    }

    public static Dialog createErrorDialog(Context context) {
        return createDialog(context, "There's an issue!, please try it again.");
    }

    public static Dialog createActionDialog(Context context, String title, String message, String btnSuccess, String btnFail, final Callbacks callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(btnSuccess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.onSuccess(null);
                    }
                })
                .setNegativeButton(btnFail, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onFail(null);
                    }
                });
        return builder.create();
    }

    public static Dialog createInputDialog(final Context context, String title, String btnSuccess, String btnFail, int typeInput, final Callbacks callback) {

        LinearLayout ll = new LinearLayout(context);
        ll.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(30, 20, 30, 0);
        final EditText input = new EditText(new ContextThemeWrapper(context, R.style.Theme_AppCompat_Light));
        input.setHint(title);
        input.setInputType(typeInput);
        ll.addView(input, layoutParams);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setView(ll)
                .setCancelable(false)
                .setPositiveButton(btnSuccess, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (input.getText().toString().trim().length() == 0) {
                            Toast.makeText(context, "Debe digitar un código.", Toast.LENGTH_SHORT).show();
                        } else {
                            callback.onSuccess(input.getText().toString().trim());
                        }
                    }
                })
                .setNegativeButton(btnFail, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onFail(null);
                    }
                });

        return builder.create();
    }

    public static void createSpinner(Context context, String title, final List<String> types, final Callbacks callback) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle(title);

        String[] types__ = new String[types.size()];
        types__ = types.toArray(types__);

        b.setItems(types__, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.onSuccess(String.valueOf(which));
            }

        });
        b.show();
    }
}
